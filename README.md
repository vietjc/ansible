I. Sử dụng ansible-galaxy geerlingguy
(Cài đặt trước 2 role này)

1. Lập trình điều kiện với condition
2. Về ansible_facts
3. Role trong ansible là cách cài đặt các ứng dụng, mỗi ứng dụng được gọi là 1 ansible role
4. Cài đặt bằng ansible-playbook k8s on prem với geerlingguy trên ubuntu 20.04



II. Hoặc sử dụng script cài đặt sau theo hướng dẫn của chatgpt
ansible-playbook -i inventory.ini install-k8s.yaml
